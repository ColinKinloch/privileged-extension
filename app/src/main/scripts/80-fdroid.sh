#!/sbin/sh
#
# ADDOND_VERSION=2
#
# /system/addon.d/80-fdroid.sh
# During a system upgrade, this script backs up F-Droid client and privext,
# /system is formatted and reinstalled, then the files are restored.
#

. /tmp/backuptool.functions

if [ -z $backuptool_ab ]; then
  SYS=$S
  TMP="/tmp"
else
  SYS="/postinstall/system"
  TMP="/postinstall/tmp"
fi

list_files() {
cat <<EOF
app/F-Droid.apk
app/F-Droid/F-Droid.apk
etc/permissions/permissions_org.fdroid.fdroid.privileged.xml
priv-app/F-DroidPrivilegedExtension.apk
priv-app/F-DroidPrivilegedExtension/F-DroidPrivilegedExtension.apk
EOF
}

case "$1" in
  backup)
    list_files | while read -r FILE DUMMY; do
      backup_file "$S"/"$FILE"
    done
  ;;
  restore)
    list_files | while read -r FILE REPLACEMENT; do
      R=""
      [ -n "$REPLACEMENT" ] && R="$S/$REPLACEMENT"
      [ -f "$C/$S/$FILE" ] && restore_file "$S"/"$FILE" "$R"
    done
  ;;
  pre-backup)
    # Stub
  ;;
  post-backup)
    # Stub
  ;;
  pre-restore)
    # Stub
  ;;
  post-restore)
    # Fix ownership/permissions
    for i in $(list_files); do
      chown root:root "$SYS/$i"
      chmod 644 "$SYS/$i"
      chmod 755 "$(dirname "$SYS/$i")"
    done
  ;;
esac
